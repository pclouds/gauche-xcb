;; -*- indent-tabs-mode: nil -*-
(use gauche.uvector)
(use gauche.vport)
(use srfi-39)
(use xcb)
(use xcb.grab-window)
(use xcb.utils)

(define (prop-value value type)
  (define (value->u32)
    (u32vector->list
     (read-uvector <u32vector>
                   (/ (uvector-length value) 4)
                   (open-input-uvector value))))

  (case (integer->atom type :strict #f)
    [(string) (u8vector->string value)]
    [(atom) (map atom->string (value->u32))]
    [(window cardinal) (value->u32)]
    [else value]))

(parameterize ([current-xcb-connection (xcb-connect)])
  (let* ([win (grab-window)]
         [prop-ids (u32vector->list
                    (ref (xcb-list-properties :window win) 'atoms))]
         [props (map (lambda (id)
                       (cons id
                             (xcb-get-property :delete 0
                                               :window win
                                               :property id
                                               :type 0
                                               :long-offset 0
                                               :long-length 100)))
                     prop-ids)])
    (for-each
     (lambda (pair)
       (let ([id (car pair)]
             [prop (cdr pair)])
         (print (atom->string id)
                "(" (atom->string (ref prop 'type)) ") = "
                (prop-value (ref prop 'value) (ref prop 'type)))))
     props))
  (xcb-disconnect))
