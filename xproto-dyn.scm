;; -*- indent-tabs-mode: nil -*-
(define-module xproto-dyn
  (use gauche.vport)
  (use srfi-39)
  (use srfi-42)
  (use srfi-60)
  (use srfi-99)
  (use xcb-gen)
  (use xcb.gen-support)
  (export-all))
(select-module xproto-dyn)

(import-xml "xproto.xml")
