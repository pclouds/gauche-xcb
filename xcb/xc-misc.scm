;; -*- indent-tabs-mode: nil -*-
(define-module xcb.xc-misc
  (use gauche.vport)
  (use scheme.bitwise)
  (use srfi-39)
  (use srfi-42)
  (use srfi-99)
  (use xcb.base)
  (use xcb.core)
  (use xcb.gen-support)
  (export-all))
(select-module xcb.xc-misc)

(define (xc-misc-supported? :optional (conn (current-xcb-connection)))
  (not (zero? (ref (cache-or-query-extension conn "XC-MISC" 'xc-misc) 'present))))

(include "gen/xc-misc.scm")
