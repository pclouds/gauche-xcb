;; -*- indent-tabs-mode: nil -*-
(define-module xcb.grab-window
  (use xcb)
  (use xcb.utils)
  (use xcb.xproto)
  (export grab-window))
(select-module xcb.grab-window)

(define (select-window)
  (define (loop proc)
    (let ([res (proc)])
      (if res res (loop proc))))

  (define (process-events)
    (xcb-allow-events :mode 'sync-pointer :time 0)
    (let ([event (xcb-read)])
      (case (ref event 'code)
        [(button-press button-release) (if (zero? (ref event 'child))
                                           (ref event 'root)
                                           (ref event 'child))]
        [else #f])))

  (define (make-crosshair-cursor)
    (let ([fid (make-xid)]
          [cid (make-xid)])
      (xcb-open-font :fid fid :name "cursor")
      (xcb-create-glyph-cursor :cid cid
                               :source-font fid
                               :mask-font fid
                               :source-char 34 ; crosshair
                               :mask-char 35 ; crosshair mask
                               :fore-red 0
                               :fore-green 0
                               :fore-blue 0
                               :back-red #xffff
                               :back-green #xffff
                               :back-blue #xffff)
      (xcb-close-font :font fid)
      cid))

  (let* ([cid (make-crosshair-cursor)]
         [status (integer->grab-status
                 (ref (xcb-grab-pointer :owner-events 0
                                        :grab-window (root-window)
                                        :event-mask '(button-press button-release)
                                        :pointer-mode 'sync
                                        :keyboard-mode 'async
                                        :confine-to (root-window)
                                        :cursor cid
                                        :time 0)
                      'status))])
    (xcb-free-cursor :cursor cid)
    (unless (eq? status 'success)
      (error "bad grab" status)))

  (let ([win (let loop () (or (process-events) (loop)))])
    (xcb-ungrab-pointer :time 0)
    win))

;; From ICCCM
;;
;; The window manager will place a WM_STATE property (of type
;; WM_STATE) on each top-level client window that is not in the
;; Withdrawn state. Top-level windows in the Withdrawn state may or
;; may not have the WM_STATE property. Once the top-level window has
;; been withdrawn, the client may re-use it for another
;; purpose. Clients that do so should remove the WM_STATE property if
;; it is still present.
;;
;; Some clients (such as xprop) will ask the user to click over a
;; window on which the program is to operate. Typically, the intent is
;; for this to be a top-level window. To find a top-level window,
;; clients should search the window hierarchy beneath the selected
;; location for a window with the WM_STATE property. This search must
;; be recursive in order to cover all window manager reparenting
;; possibilities. If no window with a WM_STATE property is found, it
;; is recommended that programs use a mapped child-of-root window if
;; one is present beneath the selected location.
(define (find-client win)
  (define (find-in-children win atom)
    (let loop ([children (ref (xcb-query-tree :window win) 'children)])
      (cond
       [(null? children)
        #f]
       [(window-has-property? (car children)
                              atom)
        (car children)]
       [else
        (loop (cdr children))])))

  (cond
   [(eq? win (root-window))
    win]
   [(window-has-property? win
                          (string->atom "WM_STATE"))
    win]
   [else (or (find-in-children win
                               (string->atom "WM_STATE"))
             win)]))


(define (grab-window)
  (find-client (select-window)))
