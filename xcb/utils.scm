;; -*- indent-tabs-mode: nil -*-
(define-module xcb.utils
  (use srfi-39)
  (use xcb.base)
  (use xcb.core)
  (use xcb.xid)
  (use xcb.xproto)
  (export-all))
(select-module xcb.utils)

;; Return pixel format at a specific depth
(define (xcb-pixmap-format-info conn depth)
  (find (lambda (f)
          (eq? (ref f 'depth) depth))
        (~ conn 'setup 'pixmap-formats)))

;; Return depth information at a specific depth
(define (xcb-depth-info screen depth)
  (find (lambda (d)
          (eq? (ref d 'depth) depth))
        (ref screen 'allowed-depths)))

;; Return visual information
(define (xcb-visual-info depth visual-id)
  (find (lambda (v)
          (eq? (ref v' visual-id) visual-id))
        (ref depth 'visuals)))

;; Return current screen
(define (current-screen)
  (~ (current-xcb-connection) 'screen))

;; Return root window id of the current screen
(define (root-window)
  (~ (current-screen) 'root))

;; Get atom name
(define (atom->string atom)
  (parameterize ([xcb-async #f])
    (ref (xcb-get-atom-name :atom atom) 'name)))

;; Look up an atom by name, optionally create one
(define (string->atom name :key (create #f))
  (parameterize ([xcb-async #f])
    (ref (xcb-intern-atom :only-if-exists (if create 0 1)
                          :name name)
         'atom)))

(define (create-simple-window :key
                              (conn (current-xcb-connection))
                              parent x y width height border-width
                              border background event-mask)
  (let ([wid (make-xid)])
    (xcb-create-window :connection conn
                       :depth 0
                       :wid wid
                       :parent parent
                       :x x
                       :y y
                       :width width
                       :height height
                       :border-width border-width
                       :class 'copy-from-parent
                       :visual 0
                       :back-pixel background
                       :border-pixel border
                       :event-mask event-mask)
    wid))

;; Check if the given id is a window
(define (valid-window? wid :key (connection (current-xcb-connection)))
  (guard (exc
          [(and (xcb-error? exc)
                (eq? (ref exc 'code) 'Window))
           #f])
    (parameterize ([xcb-async #f])
      (xcb-get-window-attributes :connection connection
                                 :window wid))))

;; Check if the given window has a specific property
(define (window-has-property? win atom)
  (parameterize ([xcb-async #f])
    (not
     (zero?
      (ref (xcb-get-property :delete 0
                             :window win
                             :property atom
                             :type 0
                             :long-offset 0
                             :long-length 0)
           'type)))))
