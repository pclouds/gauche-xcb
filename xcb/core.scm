;; -*- indent-tabs-mode: nil -*-
(define-module xcb.core
  (use binary.io)
  (use gauche.net)
  (use gauche.uvector)
  (use scheme.base)
  (use scheme.regex)
  (use srfi-39)
  (use srfi-60)
  (use srfi-98)
  (use xcb.base)
  (use xcb.gen-support)
  (use xcb.xproto)
  (export xcb-connect xcb-read xcb-disconnect cache-or-query-extension))
(select-module xcb.core)

(define (xcb-connect :optional (display-var (get-environment-variable "DISPLAY")))
  (define (parse-display-var var)
    (let ([m (regexp-matches '(seq #\:
                                   ($ (+ (/ "09")))
                                   (? #\. ($ (+ (/ "09")))))
                             var)])
      (values (string->number (m 1))
              (string->number (or (m 2) "0")))))

  (define (parse-xauthority display
                            :optional
                            (path (get-environment-variable "XAUTHORITY")))
    (define (read-length)
      (read-u16 (current-input-port) 'big-endian))

    (define (read-string length)
      (u8vector->string (read-uvector <u8vector> length)))

    (define (parse)
      (let ([family (read-u16)])
        (if (eof-object? family)
            (values "" "")
            (let* ([address (read-string (read-length))]
                   [disp (read-string (read-length))]
                   [name (read-string (read-length))]
                   [data (read-uvector <u8vector> (read-length))])
              (if (and (eq? family 1)
                       (eq? display (string->number disp)))
                  (values name data)
                  (parse))))))

    (if path
        (with-input-from-file path parse)
        (values "" "")))

  (define (endian-code)
    (case (default-endian)
      [(big-endian) #o102]
      [(little-endian) #o154]
      [(arm-little-endian) #o154]
      [else => (cut error "unrecognized endian" <>)]))

  (define (send-setup conn auth-name auth-data)
    (let ([req (make-xcb-struct-SetupRequest)])
      (slot-set! req 'byte-order (endian-code))
      (slot-set! req 'protocol-major-version 11)
      (slot-set! req 'protocol-minor-version 0)
      (slot-set! req 'authorization-protocol-name auth-name)
      (slot-set! req 'authorization-protocol-data auth-data)

      (request conn #f
               (lambda ()
                 (xcb-write-SetupRequest req)
                 #f)
               #f))
    (flush conn)
    (parameterize ([current-input-port
                    (socket-input-port (ref conn 'socket) :buffering :full)]
                   [read-count 0]
                   [pad-count 0])
      (case (peek-u8)
        [(0) (raise (xcb-read-SetupFailed))]
        [(1) (xcb-read-Setup)]
        [(2) (raise (xcb-read-SetupAuthenticate))]
        [else => (cut error "Unknown setup reply code" <>)])))

  (define (make-socket display)
    (make-client-socket 'unix
                        (string-append "/tmp/.X11-unix/X"
                                       (number->string display))))

  (receive (display screen)
      (parse-display-var display-var)
    (let* ([socket (make-socket display)]
           [conn (make <xcb-connection>)])
      (slot-set! conn 'socket socket)
      (slot-set! conn 'send request)
      (let ([setup (receive (auth-name auth-data)
                       (parse-xauthority display)
                     (send-setup conn auth-name auth-data))])
        (slot-set! conn 'setup setup)
        (slot-set! conn 'screen (list-ref (xcb-struct-Setup-roots setup)
                                          screen)))
      (slot-set! conn 'last-xid (~ conn 'setup 'resource-id-base))
      (slot-set! conn 'max-xid (bitwise-ior (~ conn 'setup 'resource-id-base)
                                            (~ conn 'setup 'resource-id-mask)))
      conn)))

(define (xcb-disconnect :optional (conn (current-xcb-connection)))
  (socket-close (ref conn 'socket))
  (slot-set! conn 'socket #f))

(define (request conn ext req reply)
  (define (prepend obj list)
    (cons obj list))

  (define (add-pending seqno proc)
    (slot-set! conn
               'pending
               (prepend (cons seqno proc)
                        (ref conn 'pending))))

  (define (send-request)
    (parameterize ([current-output-port (socket-output-port (ref conn 'socket) :buffering :full)]
                   [write-count 0])
      (req)))

  (define (get-ext-opcode ext)
    (let ([pair (assoc ext (ref conn 'extensions))])
      (unless pair
        (error "extension not initialized" ext))
      (when (zero? (ref (cdr pair) 'present))
        (error "extension not supported by server" ext))
      (ref (cdr pair) 'major-opcode)))

  (define (send-ext-request ext)
    (parameterize ([current-output-port (socket-output-port (ref conn 'socket) :buffering :full)]
                   [write-count 1])
      (write-u8 (get-ext-opcode ext))
      (req)))

  (define (get-reply seqno)
    (let loop ([result (read-from-server conn)])
      (if (and (pair? result) (eq? (car result) seqno))
          (cdr result)
          (begin
            (slot-set! conn 'parsed
                       (prepend result (ref conn 'parsed)))
            (loop (read-from-server conn))))))

  (let ([parse-reply (if ext (send-ext-request ext) (send-request))]
        [seqno (ref conn 'seqno)])
    (when parse-reply
      (add-pending seqno
                   (if (undefined? reply)
                       (cut parse-reply <>)
                       (lambda (first-byte)
                         (reply (parse-reply first-byte))))))
    (slot-set! conn
               'seqno
               ;; sequence-number is card16, wrap it back when it
               ;; gets to the limit.
               (bitwise-and (+ seqno 1) #xffff))

    (if (and (not (xcb-async)) parse-reply)
        (get-reply seqno)
        (cons seqno parse-reply))))

(define-method flush ((conn <xcb-connection>))
  (flush (socket-output-port (ref conn 'socket) :buffering :full)))

;; Read and parse data from X11 server.
;;
;; There are three types of data from X11:
;;
;; - errors raise exceptions
;;
;; - events are parsed and returned, 'xcb-event?' can be used to
;; check. xcb-<type>-event? or (ref obj 'code) could be used to
;; determine the exact event type.
;;
;; - if an event is not recognized, since the event size is known, we
;; can still safely skip it. #f will be returned.
;;
;; - replies are parsed. If a reply-handler is set, the reply-handler
;; is called and its return value plus sequence number
;; returned. Otherwise the parsed reply and sequence number are
;; returned. FIXME, should have xcb-reply? to select this group.
(define (read-from-server conn)
  (flush conn)
  (let ([code (read-u8 (socket-input-port (ref conn 'socket) :buffering :full))])
    (if (eof-object? code)
        code
        (case code
          [(0) (let ([result (parameterize ([current-input-port (socket-input-port (ref conn 'socket) :buffering :full)]
                                            [read-count 1]
                                            [pad-count 1])
                               (xcb-read-error))])
                 (slot-set! conn 'pending (alist-delete (ref result 'seqnum)
                                                        (ref conn 'pending)))
                 (raise result))]
          [(1) (parameterize ([current-input-port (socket-input-port (ref conn 'socket) :buffering :full)]
                              [read-count 4]
                              [pad-count 4])
                 (let* ([first-byte (read-u8)]
                        [seq-number (read-u16)]
                        [all-pending (ref conn 'pending)]
                        [matched (assq seq-number all-pending)])
                   (unless matched
                     (error "sequence-number not found" seq-number all-pending))
                   ;; pending list must be updated before parsing the
                   ;; reply because the reply could be a continuation
                   ;; and we will never get a chance to update
                   ;; anything afterwards.
                   (slot-set! conn 'pending (alist-delete seq-number (ref conn 'pending)))
                   (cons seq-number ((cdr matched) first-byte))))]
          [else (let ([result (parameterize ([current-input-port (socket-input-port (ref conn 'socket) :buffering :full)]
                                             [read-count 1]
                                             [pad-count 1])
                                (xcb-read-event (bitwise-and code #x7f)))])
                  result)]))))

(define (xcb-read :optional (conn (current-xcb-connection)))
  (let ([parsed (ref conn 'parsed)])
    (if (null? parsed)
        (read-from-server conn)
        (let* ([r-parsed (reverse parsed)]
               [last (car r-parsed)])
          (slot-set! conn 'parsed (reverse (cdr r-parsed)))
          last))))

(define (cache-or-query-extension conn name sym)
  (let ([rec (assoc sym (ref conn 'extensions))])
    (if rec
        (cdr rec)
        (begin
          (let ([result (parameterize ([xcb-async #f])
                          (xcb-query-extension :name name))])
            (slot-set! conn 'extensions
                       (cons (cons sym result)
                             (ref conn 'extensions)))
            result)))))
