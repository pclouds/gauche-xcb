;; -*- indent-tabs-mode: nil -*-
(define-module xcb.base
  (use srfi-39)
  (use srfi-99)
  (export-all))
(select-module xcb.base)

;; In async mode, a request that expects a reply will return a pair of
;; sequence number and the reply parser. The caller is supposed to
;; call 'read' at some point.
;;
;; If sync mode, the request will block until the reply is
;; received. Only the parsed reply will be returned. Any replies or
;; events received before are queued up and returned by 'read' later.
;;
;; If a request does not expect a reply, the return value is always #f
(define xcb-async (make-parameter #f))

(define current-xcb-connection (make-parameter #f))

(define-record-type xcb-error #t #t code seqnum bad-value minor-opcode major-opcode)

(define-record-type <xcb-event> #f xcb-event? (code xcb-event-code xcb-event-code-set!))

(define-class <xcb-connection> () (socket
                                   setup screen
                                   send
                                   (pending :init-value '())
                                   (parsed :init-value '())
                                   (seqno :init-value 0)
                                   last-xid
                                   max-xid
                                   (extensions :init-value '())))
