;; -*- indent-tabs-mode: nil -*-
(define-module xcb.xproto
  (use gauche.vport)
  (use scheme.bitwise)
  (use srfi-39)
  (use srfi-42)
  (use srfi-99)
  (use xcb.base)
  (use xcb.gen-support)
  (export-all))
(select-module xcb.xproto)

;; See xproto.xml for more information (or use ,d on gosh prompt).
;;
;; Requests:
;;
;; For requests (<request> tag), the function name is
;; xcb-<lowercase-name>. For example, xcb-create-window.
;;
;; All request parameters are keyword'd. The keyword name is the same
;; as in xproto.xml except that underscores are replaced with dashes.
;; Parameters that are of types enum or mask could take a list of
;; enums directly. Similarly a list of chars is considered a Scheme
;; string.
;;
;; All request procedures take <xcb-connection> as first argument.
;; The request is not necessarily sent to X11 after the procedure
;; returns. Flush on <xcb-connection> to make sure of that (or 'read'
;; which automatically flushes).
;;
;; Keyword argument :reply-handler could also be specified on requests
;; that expect a reply. The handler is a procedure that take the
;; parsed reply. Its returned value will be the return value of 'read'
;; function.
;;
;; Request procedures return a procedure that reads and parses data
;; from current-input-port.
;;
;; Enums:
;;
;; All enums (<enum> tag) can be represented as either one symbol or a
;; list of symbol. Two procedures enum->integer and integer->enum are
;; provided to convert betwen enum symbol(s) and integer.
(include "gen/xproto.scm")
