;; -*- indent-tabs-mode: nil -*-
(define-module xcb.xid
  (use scheme.bitwise)
  (use srfi-39)
  (use xcb.base)
  (use xcb.core)
  (use xcb.xc-misc)
  (export make-xid))
(select-module xcb.xid)

;; XID is already managed by X11 server. If we run out of XIDs, query
;; a new free range from the server via XC-MISC extension. If
;; resources are freed correctly, there should be some fresh XIDs.
(define (get-new-xid-range conn)
  (unless (xc-misc-supported? conn)
    (error "XID out of range and XC-MISC extension not supported"))
  (let ([result (parameterize ([xcb-async #f])
                  (xcb-get-xid-range :connection conn))])
    (when (and (eq? (ref result 'start-id) 0)
               (eq? (ref result 'count) 1))
      (error "X11 server reports out of XIDs" result))
    (slot-set! conn 'last-xid (ref result 'start-id))
    (slot-set! conn 'max-xid (+ (ref result 'start-id)
                                (ref result 'count)))))

;; Return a new XID. This procedure could query X11 server for free
;; XIDs if needed.
(define (make-xid :optional (conn (current-xcb-connection)))
  (let ([last (ref conn 'last-xid)])
    (if (< last (ref conn 'max-xid))
        (slot-set! conn 'last-xid (+ last 1))
        (get-new-xid-range conn)))
  (ref conn 'last-xid))
