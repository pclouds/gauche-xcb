;;
;; Package gauche-xcb
;;

(define-gauche-package "gauche-xcb"
  :version "0.0"
  :description "XCB X11 bindings\n\
                Library to communicate to X11 server."
  :require (("Gauche" (>= "0.9.8")))
  :providing-modules (xcb)
  :authors ("Duy Nguyen <pclouds@gmail.com>")
  :maintainers ()
  :licenses ("BSD")
  :repository "http://gitlab.com/pclouds/gauche-xcb.git"
  )
