;; -*- indent-tabs-mode: nil -*-
(define-module xcb
  (extend xcb.base xcb.core xcb.xid xcb.xproto))
