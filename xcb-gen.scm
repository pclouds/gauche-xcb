;; -*- indent-tabs-mode: nil -*-
(define-module xcb-gen
  (use scheme.base)
  (use scheme.bitwise)
  (use scheme.regex)
  (use sxml.ssax)
  (use sxml.sxpath)
  (use sxml.tools)
  (export xml->scheme import-xml))
(select-module xcb-gen)

(define (field-name xml-name)
  (string-map (lambda (x)
                (if (eq? x #\_) #\- x))
              xml-name))

(define (un-camel-case xml-name)
  (let ([re (regexp '(or (: (/ "AZ09") (+ (/ "az")))
                         (: (+ (/ "AZ09")) (neg-look-ahead (/ "az")))
                         (: (+ (/ "az")))))]
        [subst (lambda (m)
                 (string-append "-" (string-map char-downcase (m 0))))])
    (let ([string (regexp-replace-all re xml-name subst)])
      (substring string 1 (string-length string)))))

(define (sym . args)
  (string->symbol (apply string-append args)))

(define (get-field-names node)
  (map (lambda (x) (field-name (sxml:string-value x)))
       ((sxpath '(* @ name *text*)) node)))

(define (get-fields node)
  ((sxpath '((* (@ (or@ name align bytes))))) node))

(define (get-real-fields node)
  ((sxpath '((* (@ (or@ name))))) node))

(define (find-referer node field)
  (define fn
    (sxpath `((* (// fieldref *text* ,(lambda (node root-node var)
                                        (filter (lambda (x)
                                                  (string=? (sxml:string x) var))
                                                node)))))))
  (let ([lst (fn node node (sxml:attr field 'name))])
    (if (null? lst) #f (car lst))))

(define (getter struct field)
  (sym "xcb-" struct "-" (field-name field)))

(define (arg-getter field)
  (string->symbol (field-name field)))

(define (setter struct field)
  (sym"xcb-" struct "-" (field-name field) "-set!"))

(define (gen-scheme-op op)
    (case op
      [(&) 'bitwise-and]
      [(*) '*]
      [(/) 'quotient]
      [(+) '+]
      [else (error "unsupported op" op)]))

(define (gen-read-typedef typedef)
  (let ([oldname (sxml:attr typedef 'oldname)]
        [newname (sxml:attr typedef 'newname)])
    `(define ,(sym "xcb-read-" newname)
       ,(sym "xcb-read-"  oldname))))

(define (gen-write-typedef typedef)
  (let ([oldname (sxml:attr typedef 'oldname)]
        [newname (sxml:attr typedef 'newname)])
    `(define ,(sym "xcb-write-" newname)
       ,(sym "xcb-write-"  oldname))))

(define (gen-read-xidtype xidtype)
  (let ([name (sxml:attr xidtype 'name)])
    `(define ,(sym "xcb-read-" name)
       ,(sym "xcb-read-CARD32"))))

(define (gen-write-xidtype xidtype)
  (let ([name (sxml:attr xidtype 'name)])
    `(define ,(sym "xcb-write-" name)
       ,(sym "xcb-write-CARD32"))))

(define (gen-record-type record-name field-names)
  `(define-record-type ,(string->symbol record-name)
       (,(sym "make-" record-name))
       #t
     ,@(map (lambda (x) (list (string->symbol x))) field-names)))

(define (gen-write-object record-name fields)
  `(define-method write-object ((obj ,(string->symbol record-name)) port)
     (display ,(string-append "#<" record-name) port)
     ,@(append-map (lambda (field)
                     (let ([name (field-name (sxml:attr field 'name))])
                       `((display ,(string-append " " name "=") port)
                         ,(let ([enum (or (sxml:attr field 'enum)
                                          (sxml:attr field 'mask))]
                                [getter (sym record-name "-" name)])
                            `(display ,(if enum
                                           `(,(sym "integer->" (un-camel-case enum)) (,getter obj))
                                           `(,getter obj))
                                      port)))))
                   fields)
     (display ">" port)))

(define (gen-read-field record record-name field)
  (define (read-one type)
    (sym "xcb-read-" type))

  (define (gen-list-length lst)
    (let ([first-child (car (sxml:child-nodes lst))])
      (case (sxml:name first-child)
        [(value) (sxml:number (sxml:content first-child))]
        [(fieldref) `(,(getter record-name (sxml:string-value first-child)) ,record)]
        [(op) `(,(gen-scheme-op (sym (sxml:attr first-child 'op)))
                ,@(map (lambda (children)
                         (gen-list-length `(list ,@children)))
                       (sxml:child-nodes first-child)))]
        [else `(error "gen-list-length not supported" (quote ,first-child))])))

  (case (sxml:name field)
    [(field) (let* ([name (sxml:attr field 'name)]
                    [type (sxml:attr field 'type)]
                    [set-fn (setter record-name name)])
               `(,set-fn ,record (,(read-one type))))]
    [(pad) (if (sxml:attr field 'align)
               `(,(read-one "pad-align") ,(string->number (sxml:attr field 'align)))
               (let ([bytes (string->number (sxml:attr field 'bytes))])
                 `(,(read-one "pad") ,bytes)))]

    [(list) (let ([fields (sxml:child-nodes field)])
              (let* ([name (sxml:attr field 'name)]
                     [type (sxml:attr field 'type)]
                     [set-fn (setter record-name name)])
                `(,set-fn ,record ,(case (string->symbol type)
                                     [(char) `(xcb-read-char-list ,(gen-list-length field))]
                                     [(BYTE CARD8 KEYCODE void) `(xcb-read-u8-list
                                                                  ,(gen-list-length field))]
                                     [(ATOM CARD32 KEYSYM) `(xcb-read-u32-list
                                                             ,(gen-list-length field))]
                                     [else `(list-ec (: i ,(gen-list-length field))
                                                     (,(read-one type)))]))))]
    [(switch exprfield) '(error "switch or exprfield are not handled" field)]
    [else (error "don't know how to unpack node" field)]))

(define (gen-set-field rec first-byte type field)
  (case (sxml:name field)
    [(field) `((,(setter type (sxml:attr field 'name)) ,rec ,first-byte))]
    [(pad) '()]
    [else ("unrecognized first field" field)]))

(define (gen-op op gen-getter)
  (define (gen-operand operand)
    (case (car operand)
      [(fieldref) (gen-getter (cadr operand))]
      [(value) (string->number (cadr operand))]
      [(op) (gen-op operand getter)]
      [else ("no idea how to handle this operand" operand)]))

  (let ([operands (sxml:child-nodes op)])
    `(,(gen-scheme-op (string->symbol (sxml:attr op 'op)))
      ,(gen-operand (car operands))
      ,(gen-operand (cadr operands)))))

(define (gen-referee-value referer gen-getter)
  (case (sxml:name referer)
    [(list) (if (eq? (sxml:name (car (sxml:child-nodes referer))) 'op)
                `(error "list/op not supported" (quote ,referer))
                (case (string->symbol (sxml:attr referer 'type))
                 [(char) (let ([getter (gen-getter (sxml:attr referer 'name))])
			   `(if (string? ,getter)
                                (string-length ,getter)
                                (uvector-length ,getter)))]
                 [(BYTE CARD8 KEYCODE void) `(u8vector-length ,(gen-getter (sxml:attr referer 'name)))]
                 [(ATOM CARD32 KEYSYM) `(* 4 (u32vector-length ,(gen-getter (sxml:attr referer 'name))))]
                 [else `(length ,(gen-getter (sxml:attr referer 'name)))]))]
    [(switch) `(+
                ,@(map (lambda (enum)
                         (let ([name (string->symbol
                                      (un-camel-case
                                       (car
                                        (sxml:content enum))))]
                               [type (un-camel-case
                                      (sxml:attr enum 'ref))])
                           `(if (undefined? ,name) 0 (,(sym type "->integer") (quote ,name)))))
                       ((sxpath '(bitcase enumref)) referer)))]
    [else `(error "Unhandled referer field" (quote ,referer))]))

(define (gen-write-field container field gen-getter)
  (define (write-one type)
    (sym "xcb-write-" type))

  (define (gen-enum-value type getter)
    `(cond
      [(number? ,getter) ,getter]
      [(list? ,getter)
       (apply ,(sym (un-camel-case type) "->integer") ,getter)]
      [(symbol? ,getter)
       (,(sym (un-camel-case type) "->integer") ,getter)]
      [else => (cut error "invalid type" <>)]))

  (define (gen-bitcase field)
    (let* ([enum (un-camel-case
                  (car
                   (sxml:child-nodes
                    (car
                     ((sxpath '(enumref)) field)))))]
           [field (car ((sxpath '(field)) field))]
           [enum-field (or (sxml:attr field 'enum)
                           (sxml:attr field 'mask))]
           [getter (gen-getter enum)])
      `(unless (undefined? ,getter)
         (,(write-one (sxml:attr field 'type))
          ,(cond
            [enum-field (gen-enum-value enum-field getter)]
            [else getter])))))

  (case (sxml:name field)
    [(field) (let* ([name (sxml:attr field 'name)]
                    [type (sxml:attr field 'type)]
                    [enum (or (sxml:attr field 'enum)
                              (sxml:attr field 'mask))]
                    [referer (find-referer container field)]
                    [getter (gen-getter name)])
               `(,(write-one type) ,(cond
                                     [referer
                                      (gen-referee-value referer gen-getter)]
                                     [enum
                                      (gen-enum-value enum getter)]
                                     [else
                                      getter])))]
    [(pad) (if (sxml:attr field 'align)
               `(,(write-one "pad-align") ,(string->number (sxml:attr field 'align)))
               (let ([bytes (string->number (sxml:attr field 'bytes))])
                 `(,(write-one "pad") ,bytes)))]

    [(list) (let ([fields (sxml:child-nodes field)])
              (let* ([name (sxml:attr field 'name)]
                     [type (sxml:attr field 'type)]
                     [getter (gen-getter name)])
                (case (string->symbol type)
                  [(char) `(if (string? ,getter)
                               (xcb-write-char-list ,getter)
                               (xcb-write-u8-list ,getter))]
                  [(BYTE CARD8 KEYCODE void) `(xcb-write-u8-list ,getter)]
                  [(ATOM CARD32 KEYSYM) `(xcb-write-u32-list ,getter)]
                  [else `(for-each (lambda (x)
                                 (,(write-one type) x))
                               ,getter)])))]
    [(exprfield) `(error "exprfield not supported" (quote ,field))]
    [(switch) `(begin ,@(map gen-bitcase ((sxpath '(bitcase)) field)))]
    [else (error "don't know how to unpack node" field)]))

(define (gen-xcb-enum enum)
  (define (enum-value item)
    (let ([first-child (car (sxml:child-nodes item))])
      (case (car first-child)
        [(bit) (arithmetic-shift 1 (string->number
                                    (cadr first-child)))]
        [(value) (string->number (cadr first-child))]
        [else => (cut error "unknown enum value field" <>)])))

  (define (gen-enum->int item)
    `[(,(string->symbol (un-camel-case (sxml:attr item 'name))))
      ,(enum-value item)])

  (define (gen-int->enum-value item)
    (let ([first-child (car (sxml:child-nodes item))])
      `[(,(string->number
           (cadr first-child)))
        (quote ,(string->symbol
                 (un-camel-case
                  (sxml:attr item 'name))))]))

  ;; highly tied to integer-><enum> code generation below becaue of
  ;; the loop construct
  (define (gen-int->enum-bit item)
    (let* ([first-child (car (sxml:child-nodes item))]
           [bit-value (arithmetic-shift 1 (string->number
                                           (cadr first-child)))]
           [enum-value `(quote ,(string->symbol
                                 (un-camel-case
                                  (sxml:attr item 'name))))])
      `[(> (bitwise-and value ,bit-value) 0)
        (loop (cons ,enum-value result)
              (- value ,bit-value))]))

  (let ([enum-name (un-camel-case (sxml:attr enum 'name))])
    `((define (,(sym enum-name  "->integer") . args)
        (let loop ([result 0] [args args])
          (if (null? args)
              result
              (let ([new-result (case (car args)
                                  ,@(map gen-enum->int (get-fields enum))
                                  [else => (cut error ,(string-append "unknown " enum-name " enum") <>)])])
                (loop (+ result new-result) (cdr args))))))
      (define (,(sym "integer->" enum-name) value :key (strict #t))
        (case value
          ,@(map gen-int->enum-value ((sxpath '((item (value)))) enum))
          ,(let ([bits ((sxpath '((item (bit)))) enum)])
             (if (null? bits)
                 `[else => (lambda (x)
                             (if strict
                                 (error ,(string-append "unknown " enum-name " value") x)
                                 #f))]
                 `[else
                   (let loop ([result '()] [value value])
                     (cond
                      [(zero? value) (reverse result)]
                      ,@(map gen-int->enum-bit bits)
                      [else => (lambda (x)
                                 (if strict
                                     (error ,(string-append "unknown " enum-name " value") x)
                                     #f))]))])))))))

(define (gen-xcb-struct struct)
  (define (gen-read-struct struct)
    (let ([name (sxml:attr struct 'name)])
      `(define (,(sym "xcb-read-" name))
         (let ([struct (,(sym "make-xcb-struct-" name))])
           ,@(map (lambda (x)
                    (gen-read-field 'struct
                                    (string-append "struct-" name)
                                    x))
                  (sxml:child-nodes struct))
           struct))))

  (define (gen-write-struct struct)
    (let* ([name (sxml:attr struct 'name)]
           [gen-getter (lambda (field-name)
                         `(,(getter (string-append "struct-" name) field-name) struct))])
      `(define (,(sym "xcb-write-" name) struct)
         ,@(map (lambda (x)
                  (gen-write-field struct x gen-getter))
                (sxml:child-nodes struct)))))

  (let ([field-names (get-field-names struct)]
        [record-name (string-append "xcb-struct-" (sxml:attr struct 'name))])
    `(,(gen-record-type record-name field-names)
      ,(gen-write-object record-name (get-real-fields struct))
      ,(gen-read-struct struct)
      ,(gen-write-struct struct))))

(define (gen-read-error errors)
  (define (gen-one-code node)
    `((,(string->number (sxml:attr node 'number)))
      (quote ,(string->symbol (sxml:attr node 'name)))))

  `(define (xcb-read-error)
     (define (code->sym code)
       (case code
         ,@(map gen-one-code errors)
         [else (error "Unknown error code" code)]))

     (let* ([code (xcb-read-CARD8)]
            [sequence-number (xcb-read-CARD16)]
            [bad-value (xcb-read-CARD32)]
            [minor-opcode (xcb-read-CARD16)]
            [major_opcode (xcb-read-CARD8)])
       (xcb-read-pad 21)
       (make-xcb-error (code->sym code)
                       sequence-number
                       bad-value
                       minor-opcode
                       major_opcode))))

(define (gen-xcb-event event)
  (define (gen-event-record-type type-name fields)
    `(define-record-type (,(string->symbol type-name) <xcb-event>)
         (,(sym "make-" type-name))
         #t
       ,@(map (lambda (x) (list (string->symbol x))) fields)))

  (define (get-fields-with-seqnum event)
    (when (or (sxml:attr event 'xge)
              (sxml:attr event 'no-sequence-number))
      (error "cannot safely insert sequence-number field to this event" event))
    (let ([fields (get-fields event)])
      `(,(car fields)
        (field (@ (name "seqnum") (type "CARD16")))
        ,@(cdr fields))))

  (define (read-sym event-name)
    (sym "xcb-read-" event-name "-event"))

  (define (gen-read-event event)
    (let* ([event-name (un-camel-case (sxml:attr event 'name))]
           [name (string-append "xcb-" event-name "-event")])
      `(define (,(read-sym event-name))
         (let ([obj (,(sym "make-" name))])
           (xcb-event-code-set! obj ',(string->symbol event-name))
           ,@(map (lambda (x)
                    (gen-read-field 'obj
                                    (string-append event-name "-event")
                                    x))
                  (get-fields-with-seqnum event))
           (let ([remaining (- 32 (read-count))])
             (unless (zero? remaining)
               (xcb-read-pad remaining)))
           obj))))

  (let* ([field-names (append '("seqnum")
                              (get-field-names event))]
         [event-name (un-camel-case (sxml:attr event 'name))]
         [type-name (string-append "xcb-" event-name "-event")])
    `(,(gen-event-record-type type-name field-names)
      ,(gen-write-object type-name (append '((field (@ (name "seqnum"))))
                                           (get-real-fields event)))
      ,(case (string->symbol (sxml:attr event 'name))
         [(KeymapNotify) `(define (,(read-sym event-name))
                            (let ([rec (make-xcb-keymap-notify-event)])
                              (xcb-event-code-set! rec ',(string->symbol event-name))
                              (xcb-keymap-notify-event-keys-set!
                               rec (list-ec (: i 31) (xcb-read-CARD8)))
                              (xcb-keymap-notify-event-seqnum-set! rec #f)
                              rec))]
         [(GeGeneric) `(lambda (,(read-sym type-name))
                         (error "GeGeneric not supported yet"))]
         [else (gen-read-event event)]))))

(define (gen-xcb-read-event events)
  `(define (xcb-read-event code)
     (case code
       ,@(map (lambda (e)
                `[(,(string->number (sxml:attr e 'number)))
                  (,(sym "xcb-read-" (un-camel-case (sxml:attr e 'name)) "-event"))])
              events)
       [else (begin
               (print "Unrecognized event code " code)
               (xcb-read-pad 31)
               #f)])))

(define (gen-xcb-request request ext-sym)
  (define (gen-read-reply reply reply-name)
    (let ([fields (get-fields reply)]
          [rec-type (string-append "reply-" reply-name)])
      `(lambda (first-byte)
         (let ([obj (,(sym "make-xcb-reply-" reply-name))])
           ;; the first 4 bytes must have been read by the caller
           ,@(gen-set-field 'obj
                            'first-byte
                            (string-append "reply-" reply-name)
                            (car fields))
           (,(setter rec-type "length") obj (xcb-read-CARD32))
           ,@(map (lambda (field)
                    (gen-read-field 'obj
                                    (string-append "reply-" reply-name)
                                    field))
                  (cdr fields))
           (let ([remaining (- (+ 32 (* (,(getter rec-type "length") obj) 4))
                               (read-count))])
             (unless (zero? remaining)
               (xcb-read-pad remaining)))
           obj))))

  (define (gen-write-request request reply)
    (define (gen-write-payload request name fields)
      (map (lambda (x)
             (gen-write-field request x arg-getter))
           fields))

    (let* ([name (sxml:attr request 'name)]
           [fields ((sxpath '((* (@ (or@ name align bytes))))) request)])
      `(define (,(sym "xcb-" (un-camel-case name))
                :key
                (connection (current-xcb-connection))
                reply-handler
                ,@(map (lambda (x)
                         (string->symbol
                          (field-name x)))
                       ((sxpath '((or@ field list) @ name *text*)) request))
                ,@(map (lambda (x)
                         (string->symbol
                          (un-camel-case x)))
                       ((sxpath '(switch bitcase enumref *text*)) request)))
         ,@(if reply
               '()
               `((unless (undefined? reply-handler)
                   (error ,(string-append "This request " name " does not have a reply")))))
         ((ref connection 'send)
          connection
          ,(if ext-sym `(quote ,ext-sym) #f)
          (lambda ()
            (xcb-write-BYTE ,(string->number (sxml:attr request 'opcode)))
            ,@(cond
               [ext-sym `((let ([payload (open-output-uvector)])
                            (parameterize ([current-output-port payload]
                                           [write-count 0])
                              ,@(gen-write-payload request name fields))
                            (xcb-write-payload payload)))]
               [(null? fields) '((xcb-write-pad 1)
                                 (xcb-write-CARD16 1))]
               [else `((let ([payload (open-output-uvector)])
                         (parameterize ([current-output-port payload]
                                        [write-count 0])
                           ,@(gen-write-payload request name (cdr fields)))
                         ,(gen-write-field request (car fields) arg-getter)
                         (xcb-write-payload payload)))])
            ,reply)
          reply-handler))))

  (let ([field-names (get-field-names request)]
        [request-name (string-append "xcb-request-" (sxml:attr request 'name))]
        [reply ((if-car-sxpath '(reply)) request)]
        [reply-name (string-append "xcb-reply-" (sxml:attr request 'name))])
    `(,@(if reply
            (let ([field-names (get-field-names reply)])
              `(,(gen-record-type reply-name (cons "length" field-names))
                ,(gen-write-object reply-name (get-real-fields reply))))
            '())
      ,(gen-write-request request (if reply
                                      (gen-read-reply reply (sxml:attr request 'name))
                                      #f)))))

(define (xml->scheme)
  (let* ([sxml (ssax:xml->sxml (current-input-port) '())]
         [ext0 ((sxpath '(xcb @ extension-name *text*)) sxml)]
         [ext (and (pair? ext0) (string->symbol (un-camel-case (car ext0))))])
    `(begin
       ,@(append-map (lambda (x)
                       (list (gen-read-typedef x)
                             (gen-write-typedef x)))
                     ((sxpath '(xcb typedef)) sxml))
       ,@(append-map (lambda (x)
                       (list (gen-read-xidtype x)
                             (gen-write-xidtype x)))
                     ((sxpath '(xcb (or@ xidtype xidunion))) sxml))
       ,@(append-map gen-xcb-enum ((sxpath '(xcb enum)) sxml))
       ,@(append-map gen-xcb-struct ((sxpath '(xcb struct)) sxml))
       ,(gen-read-error ((sxpath '(xcb (or@ error errorcopy))) sxml))
       ,@(append-map gen-xcb-event ((sxpath '(xcb event)) sxml))
       ,(gen-xcb-read-event ((sxpath '(xcb event)) sxml))
       ,@(append-map (cut gen-xcb-request <> ext) ((sxpath '(xcb request)) sxml)))))

(define-macro (import-xml path)
  (with-input-from-file path xml->scheme))
