;; -*- indent-tabs-mode: nil -*-
(use gauche.uvector)
(use srfi-39)
(use xcb)
(use xcb.utils)

(define (new-window)
  (create-simple-window :parent (root-window)
                        :x 0 :y 0 :width 1 :height 1 :border-width 1
                        :border (~ (current-screen) 'black-pixel)
                        :background (~ (current-screen) 'white-pixel)))

(define (selection-notify requestor property)
  (let ([length (ref (xcb-get-property :delete 0
                                       :window requestor
                                       :property property
                                       :type 0
                                       :long-offset 0
                                       :long-length 0)
                     'bytes-after)])
    (u8vector->string
     (ref (xcb-get-property :delete 0
                            :window requestor
                            :property property
                            :type 0
                            :long-offset 0
                            :long-length length)
          'value))))

(define (read-selection selection)
  (let ([owner (xcb-get-selection-owner :selection (atom->integer selection))]
        [wid (new-window)])
    (xcb-convert-selection :requestor wid
                           :selection (atom->integer selection)
                           :target (string->atom "UTF8_STRING")
                           :property (string->atom "XCLIP_OUT" :create #t)
                           :time 0)
    (let loop ([event (xcb-read)])
      (if (eq? (ref event 'code) 'selection-notify)
          (selection-notify (ref event 'requestor)
                            (ref event 'property))
          (loop (xcb-read))))))

(parameterize ([current-xcb-connection (xcb-connect)])
  (print (read-selection 'primary)))
