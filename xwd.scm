;; -*- indent-tabs-mode: nil -*-
(use binary.io)
(use gauche.uvector)
(use srfi-39)
(use xcb)
(use xcb.grab-window)
(use xcb.utils)

(define (write-xwd-file conn
                        :key
                        drawable x y
                        width height
                        (port (current-output-port))
                        (window-name "xwd.scm"))
  (let ([image (xcb-get-image :connection conn
                              :drawable drawable
			      :x x :y y
                              :width width :height height
                              :plane-mask #xffffffff
                              :format 'z-pixmap)])
    (parameterize ([default-endian 'big-endian]
                   [current-output-port port])
      (write-u32 (+ (* 25 4) 1 (string-length window-name)))
      (write-u32 7)			; file version
      (write-u32 (image-format->integer 'z-pixmap))
      (write-u32 (ref image 'depth))
      (write-u32 width)
      (write-u32 height)
      (write-u32 0)			; x offset
      (write-u32 (~ conn 'setup 'image-byte-order))
      (write-u32 (~ conn 'setup 'bitmap-format-scanline-unit))
      (write-u32 (~ conn 'setup 'bitmap-format-bit-order))
      (let* ([depth (xcb-depth-info (ref conn 'screen)
                                    (ref image 'depth))]
             [visual (xcb-visual-info depth
                                      (ref image 'visual))]
             [format (xcb-pixmap-format-info conn
                                             (ref image 'depth))])
        (write-u32 (ref format 'scanline-pad))
        (write-u32 (ref format 'bits-per-pixel))
        (write-u32 (* width (quotient (ref format 'bits-per-pixel) 8)))
        (write-u32 (ref visual 'class))
        (write-u32 (ref visual 'red-mask))
        (write-u32 (ref visual 'green-mask))
        (write-u32 (ref visual 'blue-mask))
        (write-u32 (ref visual 'bits-per-rgb-value)))
      (write-u32 0)			; colormap-entries
      (write-u32 0)			; xwd-colors
      (write-u32 0)
      (write-u32 0)
      (write-u32 0)
      (write-u32 0)
      (write-u32 0)

      (write-string window-name)
      (write-u8 0)

      (write-uvector (ref image 'data)))))

(parameterize ([current-xcb-connection (xcb-connect)])
  (let* ([wid (grab-window)]
         [geo (xcb-get-geometry :drawable wid)])
    (with-output-to-file "a.xwd"
      (lambda ()
        (write-xwd-file (current-xcb-connection)
                        :drawable wid
                        :x 0 :y 0
                        :width (ref geo 'width)
                        :height (ref geo 'height)))))
  (xcb-disconnect))
