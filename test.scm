;; -*- indent-tabs-mode: nil -*-
(use srfi-39)
(use xcb)
(use xcb.xc-misc)

(define (dump x)
  (parameterize ([current-output-port (standard-error-port)])
    (print x)))

(parameterize ([current-xcb-connection (xcb-connect)])
  (let ([conn (current-xcb-connection)] [root #f] [wid #f])

    (print (ref conn 'setup))
    (set! root (ref (ref conn 'screen) 'root))
    (print "Root window is " root)

    (guard (e [else (print "Expected error " e)])
      (parameterize ([xcb-async #t])
        (print (xcb-get-window-attributes :reply-handler dump
                                          :window 0)))
      (xcb-read))

    (print (xcb-get-window-attributes :window root))

    (xcb-intern-atom :reply-handler dump
                     :only-if-exists 1
                     :name "_NET_WM_NAME")

    (set! wid (make-xid))
    (print (format "wid ~x" wid))
    (xcb-create-window :depth 24
                       :wid wid
                       :parent root
                       :x 100
                       :y 200
                       :width 300
                       :height 400
                       :border-width 1
                       :class 0
                       :visual 0
                       :back-pixel (ref (ref conn 'screen) 'white-pixel)
                       :event-mask '(exposure button-press button-release
                                              pointer-motion
                                              enter-window leave-window
                                              key-press key-release))

    (xcb-map-window :window wid)

    (print (xcb-get-window-attributes :window wid))

    (print (xc-misc-supported?))
    (print (xcb-get-version :client-major-version 1 :client-minor-version 0))
    (print (xcb-get-xid-range))
    (print (xcb-get-xid-list :count 10))
    (print "event loop")

    (let loop ()
      (let ([result (xcb-read)])
        (print result)
        (unless (or (eof-object? result)
                    (and (xcb-event? result)
                         (eq? (ref result 'code) 'key-press)
                         (eq? (ref result 'detail) 9)))
          (loop))))
    (xcb-disconnect)))
